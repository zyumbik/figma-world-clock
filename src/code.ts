figma.showUI(__html__, { width: 232, height: 208 })

let sel = figma.currentPage.selection


// retrieved stored text layers
figma.ui.onmessage = msg => {

	if (msg.type === 'update') {

		// let storedTexts = JSON.parse(figma.currentPage.getPluginData("storedTexts")) as Array<TextNode>

		(sel as Array<TextNode>).forEach((n, index) => {
			if (n.fontName !== figma.mixed) { // safety check
				figma.loadFontAsync(n.fontName).then(() => {
					n.characters = msg.dates[index]
				})
			}
		})
	} else if (msg.type === 'add') {

		let timezone = msg.timezone // the name of the timezone

		let tzs = [] // timezones
		// looping through each selected text layers
		sel.forEach(n => {
			if (n.type === "TEXT") {
				tzs.push(n.name)  // put their names as timezones in a array
			}
		})

		figma.ui.postMessage({
			"type": "start",
			"value": tzs  // send the timezones array to UI
		})
		// figma.currentPage.setPluginData("storedTexts", JSON.stringify(tzs)) // stores the selected layers
	} else if (msg.type === 'cancel') {
		figma.closePlugin()
		return
	}
}